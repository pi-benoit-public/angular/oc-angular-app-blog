import { Component } from '@angular/core';

import { POSTLIST } from '../post/post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html'
})
export class PostListComponent {
  postList = POSTLIST;
}
