import { Component, Input} from '@angular/core';
import { DatePipe } from '@angular/common';

import { Post } from '../post/post';

@Component({
  selector: 'app-post-list-item',
  styleUrls: ['./post-list-item.component.scss'],
  templateUrl: './post-list-item.component.html'
})
export class PostListItemComponent {
  @Input() post: Post;
  @Input() loveIts: number;

  onLoveIts(bool: boolean) {
      if (bool)
        this.loveIts = this.post.loveIts++;
      else
        this.loveIts = this.post.loveIts--;
  }
}
